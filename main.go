/*
Copyright © 2022 François Gannaz <francois.gannaz@free.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package main

import (
	_ "embed"
	"flag"
	"fmt"
	"os"

	"github.com/charmbracelet/glamour"
)

//go:embed dark-colorblind.json
var rawJSON []byte

func main() {
	flag.Parse()
	args := flag.Args()

	// If the input comes from a unix pipe, add an implicit "-" argument.
	args = addImplicitCaretArg(args)

	if len(args) == 0 {
		fmt.Println(`mdcat - Style and display a markdown file in the terminal

Just like 'cat', use with a unix pipe
or pass file names as arguments.`)
		os.Exit(0)
	}

	renderFiles(args)
}

func renderFiles(args []string) {
	renderer, err := glamour.NewTermRenderer(
		glamour.WithStylesFromJSONBytes(rawJSON),
		glamour.WithWordWrap(120),
	)
	if err != nil {
		fmt.Errorf("Renderer faild to initialize: %v", err)
		os.Exit(1)
	}

	for _, arg := range args {
		content := read(arg)
		out, err := renderer.RenderBytes(content)
		if err != nil {
			fmt.Errorf("Rendering error for '%s': %v", arg, err)
			os.Exit(1)
		}
		os.Stdout.Write(out)
	}
}

func isStdinCharDevice() bool {
	info, _ := os.Stdin.Stat()
	return (info.Mode() & os.ModeCharDevice) == os.ModeCharDevice
}

func addImplicitCaretArg(args []string) []string {
	if isStdinCharDevice() {
		return args
	}
	for _, arg := range args {
		if arg == "-" {
			return args
		}
	}
	return append(args, "-")
}

func read(arg string) []byte {
	content := make([]byte, 32_000)
	var file *os.File
	if arg == "-" {
		if isStdinCharDevice() {
			fmt.Errorf("Arg '-' was passed while STDIN is interactive")
			os.Exit(1)
		}
		file = os.Stdin
	} else {
		var err error
		file, err = os.Open(arg)
		if err != nil {
			fmt.Errorf("Cannot open file '%s': %v", arg, err)
			os.Exit(1)
		}
		defer file.Close()
	}
	size, err := file.Read(content)
	if err != nil {
		fmt.Errorf("Cannot read from file '%s': %v", arg, err)
		os.Exit(1)
	}
	if size == 32_000 {
		fmt.Errorf("File read stopped at 32kB for '%s'", arg)
	}
	return content[:size]
}
